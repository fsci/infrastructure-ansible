# FSCI Infrastructure Automation

## Usage
1. Make sure you have SSH access to the server specified in `hosts` file
1. Run this command to execute the playbook

    ```
    $ ansible-playbook -i hosts <playbook_name>.yml
    ```

## Available playbooks
1. synapse: Configure synapse and riot app
2. letsencrypt: Get LE certificates and add cron job to renew them

## Directory structure
```
├── hosts: Hosts file denoting available servers
├── roles: Various roles used by playbooks
│   └── <role name>
│       ├── tasks: Tasks to be run by role
│       │   └── main.yml
│       └── templates: Tempaltes used by the role
│           └── <template file>
└── vars: Variables used by different roles and playbooks
    └── <variable file>
```

# TODO
1. Use vault to store secrets

# License

Copyright: 2018-2019 Free Software Community of India

Everything in this repo is released under [GNU GPL version 3 (or above) License](http://www.gnu.org/licenses/gpl).
